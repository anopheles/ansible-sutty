ANSIBLE_CACHE_PLUGIN = yaml
ANSIBLE_CACHE_PLUGIN_CONNECTION = ./facts

# Gives Ansible access to SSH Agent
export

all:
	ansible-playbook --ask-vault-pass sutty-deploy.yml
